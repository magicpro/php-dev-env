version: "3"
volumes:
    # Путь к БД redis
    redis:
    # Pinba DB
    pinba:
    # Pinboard web
    pinboard:
    # Nodejs
    nodejs:

services:
    nginx:
        build:
            context: ../../images/nginx
        image: nginx
        container_name: nginx
        volumes:
            - ${WEB}:/var/www:ro
        ports:
            - "80:80"
        links:
            - fpm:fpm
            - memcached:memcached
            - logger
        networks:
            - net
        restart: unless-stopped
        logging:
            driver: "syslog"
            options:
                syslog-address: "udp://127.0.0.1:5514"
                syslog-facility: "daemon"
                tag: "nginx"

    fpm:
        build:
            context: ../../images/php/serv
            dockerfile: ./php7-fpm/Dockerfile
            args:
                allow_xdebug: ${ALLOW_XDEBUG}
        container_name: php7-fpm
        volumes:
            - ${WEB}:/var/www
            - ${DATA_XDEBUG}:/tmp/xdebug
        ports:
            - "9000:9000"
        links:
            - db:db
            - memcached:memcached
            - sphinx:sphinx
            - logger
        networks:
            - net
        restart: unless-stopped
        logging:
            driver: "syslog"
            options:
                syslog-address: "udp://127.0.0.1:5514"
                syslog-facility: "daemon"
                tag: "php7-fpm"

    crontab:
        build:
            context: ../../images/php/serv
            dockerfile: ./php7-cron/Dockerfile
            args:
                allow_xdebug: ${ALLOW_XDEBUG}
        container_name: php7-cron
        volumes:
            - ${WEB}:/var/www
            - ${DATA_XDEBUG}:/tmp/xdebug
        links:
            - db:db
            - memcached:memcached
            - sphinx:sphinx
            - logger
        networks:
            - net
        restart: unless-stopped
        logging:
            driver: "syslog"
            options:
                syslog-address: "udp://127.0.0.1:5514"
                syslog-facility: "daemon"
                tag: "php7-cron"

    db:
        build:
            context: ../../images/percona5.6
        image: percona:5.6
        container_name: percona_db
        volumes:
            - ${DATA_MYSQL}:/var/lib/mysql
        ports:
            - "11212:11212"
            - "3306:3306"
            - "9999:9999"
            - "9998:9998"
        links:
            - logger
        networks:
            - net
        environment:
            MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD}
        restart: unless-stopped
        logging:
            driver: "syslog"
            options:
                syslog-address: "udp://127.0.0.1:5514"
                syslog-facility: "daemon"
                tag: "percona"

    phpmyadmin:
        image: phpmyadmin/phpmyadmin
        container_name: phpmyadmin
        links:
            - db:db
        ports:
            - "8081:80"
        networks:
            - net
        environment:
            MYSQL_USERNAME: root
            MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD}
        restart: unless-stopped

    memcached:
        image: memcached
        container_name: memcached
        ports:
            - "11211:11211"
        links:
            - logger
        networks:
            - net
        restart: unless-stopped
        logging:
            driver: "syslog"
            options:
                syslog-address: "udp://127.0.0.1:5514"
                syslog-facility: "daemon"
                tag: "memcached"

    redis:
        build:
            context: ../../images/redis
        image: redis
        container_name: redis
        volumes:
            - ${DATA_REDIS}:/tmp/redis
        ports:
            - "6379:6379"
        links:
            - logger
        networks:
            - net
        restart: unless-stopped
        logging:
            driver: "syslog"
            options:
                syslog-address: "udp://127.0.0.1:5514"
                syslog-facility: "daemon"
                tag: "redis"

    phpredadmin:
        image: faktiva/php-redis-admin
        environment:
            - PHPREDMIN_DATABASE_REDIS_0_HOST=redis
        ports:
            - "8082:80"
        depends_on:
            - redis
        links:
            - logger
        networks:
            - net
        restart: unless-stopped
        logging:
            driver: "syslog"
            options:
                syslog-address: "udp://127.0.0.1:5514"
                syslog-facility: "daemon"
                tag: "phpredadmin"

    sphinx:
        build:
            context: ../../images/sphinx
        container_name: sphinx
        depends_on:
          - "db"
        volumes:
            - ${DATA_SPHINX}:/var/lib/sphinxsearch/data
            - ${WEB}:/var/www:ro
        ports:
            - "9312:9312"
            - "9306:9306"
        links:
            - logger
            - db
        networks:
            - net
        restart: unless-stopped
        logging:
            driver: "syslog"
            options:
                syslog-address: "udp://127.0.0.1:5514"
                syslog-facility: "daemon"
                tag: "sphinx"

    nodejs:
        build:
            context: ../../images/nodejs
        container_name: nodejs
        volumes:
            - nodejs:/var/nodejs
        ports:
            - "8085:8085"
            - "8086:8086"
        links:
            - logger
        networks:
            - net
        restart: unless-stopped
        logging:
            driver: "syslog"
            options:
                syslog-address: "udp://127.0.0.1:5514"
                syslog-facility: "daemon"
                tag: "nodejs"

    pinba:
        build:
            context: ../../images/pinba
        image: tony2001/pinba
        container_name: pinba
        volumes:
            - pinba:/local/mysql/data
        ports:
            - "30002:30002/udp"
            - "3006:3306"
        links:
            - logger
        networks:
            - net
        environment:
            MYSQL_ROOT_PASSWORD: ${MYSQL_ROOT_PASSWORD}
            PINBA_STATS_GATHERING_PERIOD: 10000
            PINBA_STATS_HISTORY: 300
            PINBA_TEMP_POOL_SIZE: 10000
            PINBA_REQUEST_POOL_SIZE: 1000000
            #PINBA_HISTOGRAM_MAX_TIME
        restart: unless-stopped
        logging:
            driver: "syslog"
            options:
                syslog-address: "udp://127.0.0.1:5514"
                syslog-facility: "daemon"
                tag: "pinba"

    pinboard:
        build:
            context: ../../images/pinboard
        container_name: pinboard
        volumes:
            - pinboard:/www
        ports:
            - "8084:8080"
        links:
            - logger
            - pinba
        networks:
            - net
        environment:
            MYSQL_HOST: "pinba"
            MYSQL_USER: "root"
            MYSQL_PASSWORD: ""
            AGGREGATION_PERIOD: 5
        restart: unless-stopped
        logging:
            driver: "syslog"
            options:
                syslog-address: "udp://127.0.0.1:5514"
                syslog-facility: "daemon"
                tag: "pinboard"

    logger:
        build:
            context: ../../images/rsyslog
        #image: voxxit/rsyslog
        container_name: logger
        volumes:
            - ${LOG_DIR}:/var/log
        ports:
            - "127.0.0.1:5514:514/udp"
        networks:
            - net
        restart: unless-stopped

networks:
    net:
        driver: bridge
        ipam:
            driver: default
            config:
                - subnet: 192.168.17.1/24
#                  aux_addresses:
#                      db: 192.168.17.33
